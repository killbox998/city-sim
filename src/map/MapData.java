package map;

import java.util.Random;

import main.Main;

public class MapData {
	Random rand = new Random();

	public void init() {
		Tiles = new MapTile[512][512];
		terrianGen();
		for (int y = 0; y < 512; y++) {
			for (int x = 0; x < 512; x++) {
				if (Tiles[x][y] == null) {
					Tiles[x][y] = MapTile.ocean;

				}
			}
		}
	}

	private void terrianGen() {
		do {
			int startx = rand.nextInt(511);
			int starty = rand.nextInt(511);
			MapTile mt = null;
			if (rand.nextBoolean())
				mt = MapTile.grass;
			else
				mt = MapTile.sand;
			Main.Map.Tiles[startx][starty] = mt;

		} while (rand.nextInt(200) != 5);

		int val =30000 ;
		//int val = 79715;
		while (val > 0) {
			int x = rand.nextInt(511);
			int y = rand.nextInt(511);
			if (Tiles[x][y] != null)
				continue;
			if(x == 0 || x == 511){
				continue;
			}
			if(y == 0 || y == 511){
				continue;
			}
			if ((Tiles[x + 1][y] == MapTile.grass || Tiles[x - 1][y] == MapTile.grass)
					|| (Tiles[x][y + 1] == MapTile.grass || Tiles[x][y - 1] == MapTile.grass)) {
				Tiles[x][y] = MapTile.grass;
				val--;
			} else if ((Tiles[x + 1][y] == MapTile.sand || Tiles[x - 1][y] == MapTile.sand)
					|| (Tiles[x][y + 1] == MapTile.sand || Tiles[x][y - 1] == MapTile.sand)) {
				Tiles[x][y] = MapTile.sand;
				val--;
			}

		}

	
	for(int y = 1;y < 511;y++){
		for(int x = 1;x < 511;x++){
			if(Tiles[x][y] == null){
				if(Tiles[x + 1][y] != null && Tiles[x - 1][y] != null){
					if(Tiles[x][y + 1] != null && Tiles[x][y - 1] != null){
						Tiles[x][y] = Tiles[x+1][y];
					}
				}
			}
		}
		
	}
	
	}

	public MapTile[][] Tiles;


public void placeBlock(MapTile mt, int x, int y){
	Tiles[x][y] = mt;
	
}

public int sx;
public int sy;
public int spy, spx;

public void generateRoad(int StartX, int StartY, int EndX, int EndY){
	if(StartX == EndX && StartY == EndY){
		return;
	}
	if( StartX - EndX == 0){
		if(StartY - EndY < 0){
			for(int y = StartY;y < EndY - StartY;y++){
				Tiles[StartX][y] = MapTile.road;
			}
		}
		if(StartY - EndY > 0){
			for(int y = StartY;y > EndY - StartY;y--){
				Tiles[StartX][y] = MapTile.road;
			}
		}
		
	}
}


public void fill(int startX, int startY, int stopX, int stopY,int corner){
	switch(corner){
	case 1:
		for(int y = startY;y < stopY;y++){
			for(int x = startX;x < stopX;x++){
				Main.Map.Tiles[x][y] = MapTile.road;
			}
		}
		break;
	case 2:
		for(int y = startY;y < stopY;y++){
			for(int x = stopX;x < startX;x++){
				Main.Map.Tiles[x][y] = MapTile.road;
			}
		}
		break;
	case 3:
		for(int y = stopY;y < startY;y++){
			for(int x = startX;x < stopX;x++){
				Main.Map.Tiles[x][y] = MapTile.road;
			}
		}
		break;
	case 4:
		for(int y = stopY;y < startY;y++){
			for(int x = stopX;x < startX;x++){
				Main.Map.Tiles[x][y] = MapTile.road;
			}
		}
		break;
	}
	
}

}
