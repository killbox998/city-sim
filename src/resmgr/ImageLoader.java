package resmgr;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.Main;



public class ImageLoader {
	public static BufferedImage EmptyMap;
	public static BufferedImage Checker;
	public static BufferedImage MenuBack;
	 public static BufferedImage[] Textures;
	 public static BufferedImage[] numbers;
	 public static void menuInit(){
		 try {
				
				MenuBack = ImageIO.read(ImageLoader.class.getResourceAsStream("BackGround.png"));
			} catch (IOException e) {
						e.printStackTrace();
					}
		
		
	 }
	public static void init(){
		Textures = new BufferedImage[256];
		numbers = new BufferedImage[64];
		BufferedImage io = null;
		BufferedImage io2 = null;
		try {
	io = ImageIO.read(ImageLoader.class.getResourceAsStream("Textures.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(int y = 0;y < 16;y++){
			for(int x = 0;x < 16;x++){
				Textures[y*16 + x] = io.getSubimage(x*16, y*16, 16, 16);
			}
		}
		try {
			io2 = ImageIO.read(ImageLoader.class.getResourceAsStream("numbers.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				for(int y = 0;y < 8;y++){
					for(int x = 0;x < 8;x++){
						numbers[y*8 + x] = io2.getSubimage(x*32, y*32, 32, 32);
					}
				}
		try {
			EmptyMap = ImageIO.read(ImageLoader.class.getResourceAsStream("EmptyMap.png"));
			Checker = ImageIO.read(ImageLoader.class.getResourceAsStream("Checker.png"));
			Main.hud.FPS = ImageIO.read(ImageLoader.class.getResourceAsStream("fps.png"));
			Main.render.hud = ImageIO.read(ImageLoader.class.getResourceAsStream("hud.png"));
			
		} catch (IOException e) {
					e.printStackTrace();
				}
	
	
	
	
	}




public static BufferedImage intParser(int i){
	String s = Integer.toString(i);
	
	int len = s.length();
	System.out.println(len);
	BufferedImage bi = new BufferedImage(len*30,32, BufferedImage.TYPE_INT_ARGB);
	Graphics g = bi.getGraphics();
	for(int x = 0;x	<len;x++){
		g.drawImage(digitIntParser(s.charAt(x)), x*30, 0, null);
		
	}
	
	
	return bi;
}
public static BufferedImage digitIntParser(char i){
	if(i == 45){
		i = 58;
	}
	return ImageLoader.numbers[i-48];
	
	
}




}
