package main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import map.MapTile;

public class GameInput implements KeyListener, MouseMotionListener,
		MouseListener {

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == e.VK_UP) {
			Main.Up = true;
		}
		if (e.getKeyCode() == e.VK_DOWN) {
			Main.Down = true;
		}
		if (e.getKeyCode() == e.VK_LEFT) {
			Main.Left = true;

		}
		if (e.getKeyCode() == e.VK_RIGHT) {
			Main.Right = true;
		}

		if (e.getKeyCode() == e.VK_R) {
			Main.reset();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if (e.getKeyCode() == e.VK_UP) {
			Main.Up = false;
		}
		if (e.getKeyCode() == e.VK_DOWN) {
			Main.Down = false;
		}
		if (e.getKeyCode() == e.VK_LEFT) {
			Main.Left = false;
		}
		if (e.getKeyCode() == e.VK_RIGHT) {
			Main.Right = false;
		}

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		int x2 = e.getX();
		int y2 = e.getY();
	//	x += -Main.render.xOffSet;
	   // y += -Main.render.yOffSet;
		x /= 16;
		y /= 16;
		x2 /= 16;
		y2 /= 16;


		Main.x = x2;
		Main.y = y2;
		Main.mapRend.x = x;
		Main.mapRend.y = y;

	}

	@Override
	public void mouseMoved(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		//x += -Main.render.xOffSet;
		//y += -Main.render.yOffSet;
		x /= 16;
		y /= 16;

		Main.mapRend.x = x;
		Main.mapRend.y = y;
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getButton() == e.BUTTON1) {
			Main.terriangen = true;

		}
		if(e.getButton() == e.BUTTON3){
			Main.fillX = (e.getX())/16;
			Main.fillY = (e.getY() )/16;
		}

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		
		if (e.getButton() == e.BUTTON1) {
			Main.terriangen = false;
		}
		if(e.getButton() == e.BUTTON3){
			Main.fillX2 = (e.getX() )/16;
			Main.fillY2 = (e.getY() )/16;
			if(Main.fillX2 - Main.fillX > 0 && Main.fillY2 - Main.fillY > 0 ){
				 Main.Corner = 4;
			}
			if(Main.fillX2 - Main.fillX < 0 && Main.fillY2 - Main.fillY < 0 ){
				 Main.Corner = 1;
			}
			if(Main.fillX2 - Main.fillX > 0 && Main.fillY2 - Main.fillY < 0 ){
				 Main.Corner = 2;
			}
			if(Main.fillX2 - Main.fillX > 0 && Main.fillY2 - Main.fillY < 0 ){
				 Main.Corner = 3;
			}
			Main.Map.fill(Main.fillX, Main.fillY, Main.fillX2, Main.fillY2, Main.Corner);
		
		}
	}
}
