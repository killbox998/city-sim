package main;

import map.MapData;
import map.MapTile;
import resmgr.ImageLoader;
import screen.HUD;
import screen.MapRender;
import screen.Render;

public class Main implements Runnable{
	volatile public static boolean Up, Down, Left, Right, GameLoaded;
	public static MapData Map = new MapData();
	public static MapRender mapRend = new MapRender();
	volatile public static boolean terriangen;
	public static HUD hud = new HUD();
	volatile public static boolean a = false;
	boolean b = false;
	static Thread t;
	static Thread r;
	volatile public static int speed = 30;
	public static Render render = new Render();
	volatile public static int x, y, fillX, fillY, fillX2, fillY2, Corner;
	volatile public  static int fps1 = 0, fps2 = 0, fps3 = 0, fps = 0;
	volatile public static long frames = 0;
	volatile public static int frameb = 0, frameb2 = 0, frameb3 = 0;
	volatile public static boolean mousemoving;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
	render.init();
	ImageLoader.menuInit();
	MainMenu.init();
	
	t = new Thread(new Main());
	t.start();
	while(!GameLoaded){
		render.mrender();
	}
	loop();

	}

	private static void init() {
		r = new Thread(new Reset());
		r.start();
		ImageLoader.init();
		System.out.println("sup1");
		MapTile.init();
		System.out.println("sup2");
		Map.init();
		System.out.println("sup3");
		mapRend.init();
		System.out.println("sup4");
		hud.init();
		System.out.println("sup5");
		
		
	}

	static void loop() {
		
		long before;
		long after;
		int time;
		
		while(true){
			
		while(GameLoaded){
		boolean hasrun = true;
			before = System.currentTimeMillis();
			
			hud.update();
			render.render();
			if(Up){
				up();
			}
			if(Down){
				down();
			}
			if(Left){
				left();
			}
			
			if(Right){
				right();
			}
		
		
		if(terriangen){
			Map.placeBlock(MapTile.road, x, y);
			
		}
		after = System.currentTimeMillis();
		time = (int) (after - before);
		
		if(frameb == 0){
			if(time != 0)
		fps1 = 1000 / time;
		frameb++;
		hasrun = false;
		}
		if(frameb == 1 && hasrun == true){
			if(time != 0)
			fps2 = 1000 / time;
			hasrun = false;
			frameb++;
		
			
			}
		if(frameb == 2 && hasrun == true){
			if(time != 0)
			fps3 = 1000 / time;
			
			frameb = 0;
			frameb2++;
			if(frameb2 == 6){
				fps = fps1 + fps2 + fps3/3;
				frameb2 = 0;
			}
			
		}
		frames++;
		if(frames > 50){
			speed = 512*16/(fps*2);
		}
		mapRend.update();
		frameb3++;
		if(frameb3 == 4){
			frameb3 = 0;
		}
		}
		
		
		
		}
		
		
		
		
		
		
		
	}

public static void up(){
	render.yOffSet +=speed;

}
public static void down(){
	render.yOffSet -=speed;
}
public static void left(){
	render.xOffSet +=speed;
}
public static void right(){
	render.xOffSet -=speed;
}

@Override
public void run() {
	
	init();
	GameLoaded = true;
}

public static void reset() {
	Reset.res = true;
	
}


}
