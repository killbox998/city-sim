package screen;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import main.GameInput;
import main.Main;
import main.MainMenu;

import resmgr.ImageLoader;
//Window width = 16 Blocks,height = 16 Blocks
public class Render{
	BufferedImage map;
	public BufferedImage hud;
	Graphics g;
	JFrame frame;
	public int xOffSet = 0;
	public int yOffSet = 0;
	private BufferStrategy bs;
	public void init(){
		frame = new JFrame("City Sim");
		frame.setSize(1024, 1024);
		frame.addKeyListener(new GameInput());
		frame.addMouseMotionListener(new GameInput());
		frame.addMouseListener(new GameInput());
		frame.setVisible(true);
		frame.createBufferStrategy(3);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	public void render(){
		bs = frame.getBufferStrategy();
		g = bs.getDrawGraphics();
		g.drawImage(ImageLoader.Checker, 0, 0,null);
		g.drawImage(ImageLoader.Checker, 1024, 0,null);
		g.drawImage(map, xOffSet, yOffSet,null);
		g.drawImage(hud, 0, 0,null);
		
		
		bs.show();
	}
	public void mrender(){
		bs = frame.getBufferStrategy();
		g = bs.getDrawGraphics();
		g.drawImage(MainMenu.background, 0, 0,null);
	
		
		yOffSet = xOffSet = 0;
		bs.show();
	}

}
